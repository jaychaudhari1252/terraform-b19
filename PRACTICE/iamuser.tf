provider "aws" {
  region = "ap-south-1"  
}

resource "aws_iam_user" "my_iam_user" {
  name = "JIO5G"
  path = "/"
  tags = {
    Name = "jio5g"
  }
}

output "iam_user_name" {
  value = aws_iam_user.my_iam_user.name
}
