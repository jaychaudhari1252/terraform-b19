provider "aws" {
region = "ap-south-1"
}
resource "aws_s3_bucket" "my_bk" {
  bucket = "my-bucket925925"
  acl = "private"
  tags = {
    Name = "my-bucket925925"
  }
}

resource "aws_iam_policy" "s3_bucket_policy" {
  name        = "S3BucketPolicyForMyBucket"
  description = "IAM policy for granting PutBucketPolicy action on the S3 bucket"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "s3:PutBucketPolicy",
        Resource = "arn:aws:s3:::my-bucket925925"
      }
    ]
  })
}
