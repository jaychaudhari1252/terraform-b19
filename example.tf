resource "aws_instance" "new_instance" {
    ami = ami-03f4878755434977f
    instance_type = "t2.micro"
    key-name = "jay1"
    vpc_security_group_ids = [ "sg-0ca897c1e9baa6771" ]
    tags = {
        Name = "my-instance"
        env = "dev"
    }
}