provider "aws" {
    region = "sa-east-1"
}

terraform {
    backend "s3" {
        region = "sa-east-1"
        bucket = "sam-sum-bucket-01"
        key    = "terraform.tfstate"
    }
}

data "aws_security_group" "existing" {
    name   = "new-sg"
    vpc_id = "vpc-0467550b4096cce59"
}

resource "aws_security_group_rule" "allow-httpd" {
    type              = "ingress"
    from_port         = 80
    to_port           = 80
    protocol          = "tcp"
    cidr_blocks       = ["0.0.0.0/0"]
    security_group_id = data.aws_security_group.existing.id
}

resource "aws_security_group_rule" "allow-ssh" {
    type              = "ingress"
    from_port         = 22
    to_port           = 22
    protocol          = "tcp"
    cidr_blocks       = ["0.0.0.0/0"]
    security_group_id = data.aws_security_group.existing.id
}

resource "aws_instance" "my-india" {
    ami                    = "ami-05674a88169b69e3b"
    instance_type          = "t2.micro"
    key_name               = "jay1-universal-key"
    vpc_security_group_ids = [data.aws_security_group.existing.id]
    
    tags = {
        Name = "jay-instance"
    }

    connection {
        type        = "ssh"
        user        = "ec2-user"
        private_key = file("./jay1.pem")
        host        = self.public_ip
    }

   provisioner "remote-exec" {
    inline = [
        "sudo yum install httpd -y || exit 1",
        "sudo systemctl start httpd",
        "sudo systemctl enable httpd",
        "sudo mv /var/www/html/index.html /var/www/html/index.html.bak",  # Backup the default index.html
        "sudo mv /var/www/html/index.html /var/www/html/index.html.default",  # Backup the default index.html
        "sudo mv /var/www/html/index.html /var/www/html/index.html.default",  # Backup the default index.html
        "sudo cp /var/www/html/index.html.default /var/www/html/index.html", # Copy our custom index.html
        "sudo chmod 644 /var/www/html/index.html",  # Set appropriate permissions
        "sudo chown root:root /var/www/html/index.html",  # Set appropriate ownership
        "sudo systemctl restart httpd" 
    ]
}
    provisioner "local-exec" {
    command = "sudo echo '<h1>Hello World</h1>' > index.html""
}


    provisioner "file" {
        source      = "index.html"
        destination = "/var/www/html/index.html" 
        }
}